import React from 'react';

import './Cards.css';


class Cards extends React.Component {
  constructor(props) {
    super(props);
  }

  
  
  onClick (event) {
    window.open('/product?id='+event.target.value, "_self");
    console.log(event.target.value)
  }


  render() {
    return (

      <div className="row">

          
          {this.props.items.map(
                  (item,index) =>
                      (
                        <div className="col-4">
                        <div class="card booking-card">
                        
                          
                          <div class="view overlay">
                            <img class="card-img-top" src={"http://localhost/Ecommerce/images/"+item.portada} alt="Card image cap"></img>
                            <a href="#!">
                              <div class="mask rgba-white-slight"></div>
                            </a>
                          </div>
                        
                          
                          <div class="card-body">
                        
                            
                            <h4 class="card-title font-weight-bold"><a>{item.nombre}</a></h4>
                            

                            <strong><p class="mb-2">${item.precio}</p></strong>
                            
                            
                  
                            <hr class="my-4"></hr>
                            
                            
                               <p><div class="chip mr-0">Autor: {item.autor}</div></p>
                              
                                <p><div class="chip mr-0">Editorial: {item.editorial}</div></p>
                             
                                <p><div class="chip mr-0">Genero: {item.genero}</div></p>
                             
                            <button type="button" class="btn btn-info btn-rounded" value={item.id}
                              onClick={event => this.onClick(event)}>Ver mas detalles</button>
                        
                          </div>
                        
                        </div>
                        </div>
                  
                    )
                  )
          }

</div>
          
        
    

  );
  }
 }
export default Cards;