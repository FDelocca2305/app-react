import React from 'react';

import "./Product.css";


class Product extends React.Component {
    constructor(props){
        super(props);
        this.state={
            producto:[]
        }
    }

    

    componentDidMount () {
        var url = 'http://localhost/Ecommerce/api/producto.php'+window.location.search;
        fetch(url)
        .then(response => response.json())
        .then(data => {
          this.setState({
            producto: data.response
          });
          
        })
        .catch(err => {
          console.log(err);
        })
        
      }

    render(){
        return(
        <div>
            {this.state.producto.map(
                  (elemento,index) =>
                      (
                        <div>
                        <div className="section product-header">
                            <div className="container">
                            <div className="columns">
                                <div className="column">
                                <h1 className="title is-3">{elemento.nombre}</h1>
                                
                                </div>
                            </div>
                            </div>
                        </div>

                        <div className="section">
                            <div className="container">
                            <div className="columns">
                                <div className="column is-6">
                                <div className="image is-2by2">
                                    <img src={"http://localhost/Ecommerce/images/"+elemento.portada}></img>
                                </div>
                                </div>
                                <div className="column is-5 is-offset-1">
                                
                                <p className="title is-3 has-text-muted precio">${elemento.precio}</p>
                                <hr></hr>
                                <br></br>
                                <p>
                                    Genero: {elemento.genero}
                                </p>
                                <p>
                                    Editorial: {elemento.editorial}
                                </p>
                                <p>
                                    Autor: {elemento.autor}
                                </p>
                                
                                <br></br>
                                <p>   {elemento.sinopsis}
                                </p>
                                <br></br>
                                <br></br>
                                <p className="">
                                    
                                    &nbsp;
                                    <input type="text" name="" className="input has-text-centered" value="1"></input>
                                    &nbsp;
                                    
                                    &nbsp; &nbsp; &nbsp;
                                    <a className="button is-primary">Add to cart</a>
                                </p>
                                <br></br>
                               
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                  
                    )
                  )}
            </div>
            
        );
    }
}

export default Product;