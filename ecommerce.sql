-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-10-2019 a las 01:21:07
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ecommerce`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mangacatalogo`
--

CREATE TABLE `mangacatalogo` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `autor` text COLLATE utf8_spanish_ci NOT NULL,
  `genero` text COLLATE utf8_spanish_ci NOT NULL,
  `idgenero` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `editorial` text COLLATE utf8_spanish_ci NOT NULL,
  `sinopsis` text COLLATE utf8_spanish_ci NOT NULL,
  `portada` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mangacatalogo`
--

INSERT INTO `mangacatalogo` (`id`, `nombre`, `autor`, `genero`, `idgenero`, `precio`, `editorial`, `sinopsis`, `portada`) VALUES
(1, 'Berserk Tomo 1', 'Kentaro Miura', 'Seinen', 1, 280, 'Panini Manga', '<p>Guts, un ex mercenario conocido como &quot;espadach&iacute;n negro&quot;, est&aacute; buscando venganza. Luego de una tumultuosa infancia, &eacute;l finalmente encuentra a alguien a quien respeta y en quien puede confiar; todo se desmorona cuando esta persona le arrebata todo lo importante para &eacute;l con el prop&oacute;sito de cumplir sus propios deseos. Ahora, marcado con la muerte, Guts se encuentra condenado a un destino en el que es implacablemente perseguido por seres demon&iacute;acos. Encaminado en una terrible misi&oacute;n llena de desgracia, Guts, armado con una espada gigante y una fuerza monstruosa, no dejar&aacute; que nada lo detenga, ni siquiera la misma muerte, hasta que sea capaz de tomar la cabeza de aqu&eacute;l que le rob&oacute; toda su humanidad.</p>', 'berserkt1.jpg'),
(2, 'Berserk Tomo 2', 'Kentaro Miura', 'Seinen', 1, 280, 'Panini Manga', '<p>Guts, un ex mercenario conocido como &quot;espadach&iacute;n negro&quot;, est&aacute; buscando venganza. Luego de una tumultuosa infancia, &eacute;l finalmente encuentra a alguien a quien respeta y en quien puede confiar; todo se desmorona cuando esta persona le arrebata todo lo importante para &eacute;l con el prop&oacute;sito de cumplir sus propios deseos. Ahora, marcado con la muerte, Guts se encuentra condenado a un destino en el que es implacablemente perseguido por seres demon&iacute;acos. Encaminado en una terrible misi&oacute;n llena de desgracia, Guts, armado con una espada gigante y una fuerza monstruosa, no dejar&aacute; que nada lo detenga, ni siquiera la misma muerte, hasta que sea capaz de tomar la cabeza de aqu&eacute;l que le rob&oacute; toda su humanidad.</p>dad.', 'berserkt2.jpg'),
(3, 'Berserk Tomo 3', 'Kentaro Miura', 'Seinen', 1, 280, 'Panini Manga', '<p>Guts, un ex mercenario conocido como &quot;espadach&iacute;n negro&quot;, est&aacute; buscando venganza. Luego de una tumultuosa infancia, &eacute;l finalmente encuentra a alguien a quien respeta y en quien puede confiar; todo se desmorona cuando esta persona le arrebata todo lo importante para &eacute;l con el prop&oacute;sito de cumplir sus propios deseos. Ahora, marcado con la muerte, Guts se encuentra condenado a un destino en el que es implacablemente perseguido por seres demon&iacute;acos. Encaminado en una terrible misi&oacute;n llena de desgracia, Guts, armado con una espada gigante y una fuerza monstruosa, no dejar&aacute; que nada lo detenga, ni siquiera la misma muerte, hasta que sea capaz de tomar la cabeza de aqu&eacute;l que le rob&oacute; toda su humanidad.</p>', 'berserkt3.jpg'),
(4, 'Berserk Tomo 4', 'Kentaro Miura', 'Seinen', 1, 280, 'Panini Manga', '<p>Guts, un ex mercenario conocido como &quot;espadach&iacute;n negro&quot;, est&aacute; buscando venganza. Luego de una tumultuosa infancia, &eacute;l finalmente encuentra a alguien a quien respeta y en quien puede confiar; todo se desmorona cuando esta persona le arrebata todo lo importante para &eacute;l con el prop&oacute;sito de cumplir sus propios deseos. Ahora, marcado con la muerte, Guts se encuentra condenado a un destino en el que es implacablemente perseguido por seres demon&iacute;acos. Encaminado en una terrible misi&oacute;n llena de desgracia, Guts, armado con una espada gigante y una fuerza monstruosa, no dejar&aacute; que nada lo detenga, ni siquiera la misma muerte, hasta que sea capaz de tomar la cabeza de aqu&eacute;l que le rob&oacute; toda su humanidad.</p>', 'berserkt4.jpg'),
(5, 'Berserk Tomo 5', 'Kentaro Miura', 'Seinen', 1, 280, 'Panini Manga', '<p>Guts, un ex mercenario conocido como &quot;espadach&iacute;n negro&quot;, est&aacute; buscando venganza. Luego de una tumultuosa infancia, &eacute;l finalmente encuentra a alguien a quien respeta y en quien puede confiar; todo se desmorona cuando esta persona le arrebata todo lo importante para &eacute;l con el prop&oacute;sito de cumplir sus propios deseos. Ahora, marcado con la muerte, Guts se encuentra condenado a un destino en el que es implacablemente perseguido por seres demon&iacute;acos. Encaminado en una terrible misi&oacute;n llena de desgracia, Guts, armado con una espada gigante y una fuerza monstruosa, no dejar&aacute; que nada lo detenga, ni siquiera la misma muerte, hasta que sea capaz de tomar la cabeza de aqu&eacute;l que le rob&oacute; toda su humanidad.</p>', 'berserkt5.jpg'),
(6, 'One Punch-Man Tomo 1', 'One', 'Seinen', 1, 280, 'Ivrea', '&iexcl;Sigue la vida de un h&eacute;roe promedio que gana todas sus peleas con un solo pu&ntilde;o! Esto le causa un mont&oacute;n de frustraci&oacute;n, ahora ya no siente la adrenalina y la emoci&oacute;n de una dura pelea. Tal vez ese riguroso entrenamiento para volverse fuerte no vali&oacute; la pena. Despu&eacute;s de todo, &iquest;qu&eacute; tiene de bueno tener un poder tan aplastante?', 'onepuncht1.jpg'),
(7, 'One Punch Man Tomo 5', 'One', 'Seinen', 1, 280, 'Ivrea', '&iexcl;Sigue la vida de un h&eacute;roe promedio que gana todas sus peleas con un solo pu&ntilde;o! Esto le causa un mont&oacute;n de frustraci&oacute;n, ahora ya no siente la adrenalina y la emoci&oacute;n de una dura pelea. Tal vez ese riguroso entrenamiento para volverse fuerte no vali&oacute; la pena. Despu&eacute;s de todo, &iquest;qu&eacute; tiene de bueno tener un poder tan aplastante?', 'onepuncht5.jpg'),
(8, 'One Punch Man Tomo 10', 'One', 'Seinen', 1, 280, 'Ivrea', '&iexcl;Sigue la vida de un h&eacute;roe promedio que gana todas sus peleas con un solo pu&ntilde;o! Esto le causa un mont&oacute;n de frustraci&oacute;n, ahora ya no siente la adrenalina y la emoci&oacute;n de una dura pelea. Tal vez ese riguroso entrenamiento para volverse fuerte no vali&oacute; la pena. Despu&eacute;s de todo, &iquest;qu&eacute; tiene de bueno tener un poder tan aplastante?', 'onepuncht10.jpg'),
(9, 'One Punch-Man Tomo 15', 'One', 'Seinen', 1, 280, 'Ivrea', '&iexcl;Sigue la vida de un h&eacute;roe promedio que gana todas sus peleas con un solo pu&ntilde;o! Esto le causa un mont&oacute;n de frustraci&oacute;n, ahora ya no siente la adrenalina y la emoci&oacute;n de una dura pelea. Tal vez ese riguroso entrenamiento para volverse fuerte no vali&oacute; la pena. Despu&eacute;s de todo, &iquest;qu&eacute; tiene de bueno tener un poder tan aplastante?', 'onepuncht15.jpg'),
(10, 'Slam Dunk Tomo 1 (Kanzenban)', 'Takehiko Inoue', 'Shounen', 2, 595, 'Ivrea', '<p>Slam Dunk se centra en la vida de Hanamichi Sakuragi, un pandillero que se apunta al equipo de baloncesto para intentar ligarse a una chica de su secundaria. Una compleja historia con ingredientes rom&aacute;nticos, c&oacute;micos y de autosuperaci&oacute;n, en la que el protagonista poco a poco ir&aacute; descubriendo la pasi&oacute;n por este deporte, m&aacute;s all&aacute; de las razones por las que entr&oacute; inicialmente en el equipo. Hanamichi tendr&aacute; tres grandes problemas para conquistar a la dulce y amable Haruko: su compa&ntilde;ero de equipo y rival a muerte, el gran Kaede Rukawa, el amor platonico de Haruko; el capit&aacute;n del equipo hermano de Haruko y su car&aacute;cter que es el problema m&aacute;s grande a lo hora de las practicas dentro del equipo. As&iacute; comienza un Spokon en el que habr&aacute; mucha comedia, partes de acci&oacute;n, personajes carism&aacute;ticos e inolvidables y sobre todo, partidos emocionantes de Baloncesto.</p>', 'slamdunkt1.jpg'),
(11, 'Slam Dunk Tomo 2 (Kanzenban)', 'Takehiko Inoue', 'Shounen', 2, 595, 'Ivrea', '<p>Slam Dunk se centra en la vida de Hanamichi Sakuragi, un pandillero que se apunta al equipo de baloncesto para intentar ligarse a una chica de su secundaria. Una compleja historia con ingredientes rom&aacute;nticos, c&oacute;micos y de autosuperaci&oacute;n, en la que el protagonista poco a poco ir&aacute; descubriendo la pasi&oacute;n por este deporte, m&aacute;s all&aacute; de las razones por las que entr&oacute; inicialmente en el equipo. Hanamichi tendr&aacute; tres grandes problemas para conquistar a la dulce y amable Haruko: su compa&ntilde;ero de equipo y rival a muerte, el gran Kaede Rukawa, el amor platonico de Haruko; el capit&aacute;n del equipo hermano de Haruko y su car&aacute;cter que es el problema m&aacute;s grande a lo hora de las practicas dentro del equipo. As&iacute; comienza un Spokon en el que habr&aacute; mucha comedia, partes de acci&oacute;n, personajes carism&aacute;ticos e inolvidables y sobre todo, partidos emocionantes de Baloncesto.</p>', 'slamdunkt2.jpg'),
(12, 'Slam DUnk Tomo 3 (Kanzenban)', 'Takehiko Inoue', 'Shounen', 2, 595, 'Ivrea', '<p>Slam Dunk se centra en la vida de Hanamichi Sakuragi, un pandillero que se apunta al equipo de baloncesto para intentar ligarse a una chica de su secundaria. Una compleja historia con ingredientes rom&aacute;nticos, c&oacute;micos y de autosuperaci&oacute;n, en la que el protagonista poco a poco ir&aacute; descubriendo la pasi&oacute;n por este deporte, m&aacute;s all&aacute; de las razones por las que entr&oacute; inicialmente en el equipo. Hanamichi tendr&aacute; tres grandes problemas para conquistar a la dulce y amable Haruko: su compa&ntilde;ero de equipo y rival a muerte, el gran Kaede Rukawa, el amor platonico de Haruko; el capit&aacute;n del equipo hermano de Haruko y su car&aacute;cter que es el problema m&aacute;s grande a lo hora de las practicas dentro del equipo. As&iacute; comienza un Spokon en el que habr&aacute; mucha comedia, partes de acci&oacute;n, personajes carism&aacute;ticos e inolvidables y sobre todo, partidos emocionantes de Baloncesto.</p>', 'slamdunkt3.jpg'),
(13, 'Fushi Yuugi Tomo 1', 'Yuu Watase', 'Shoujo', 5, 280, 'Ivrea', 'La joven Miaka y su amiga Yugi viajan al mundo m&aacute;gico a causa de la lectura de un misterioso libro. All&iacute;, un apuesto joven llamado Tamahome las salva de unos delincuentes. Sin embargo, Yugi desaparece mientras Miaka descubre que es la sacerdotisa de Suzaku, la &uacute;nica persona capaz de traer la paz al reino de Kohnan gracias a la ayuda de siete compa&ntilde;eros con poderes m&aacute;gicos. Lo que no sabe nuestra protagonista es que para conseguir su prop&oacute;sito deber&aacute; enfrentarse a su amiga de la infancia: Yui, quien ha sido coronada como la sacerdotisa de Seiryu, clan rival de Suzaku. Empieza una aventura m&aacute;gica inolvidable...', 'fushigit1.jpg'),
(14, 'Fushigi Yuugi Tomo 2', 'Yuu Watase', 'Shoujo', 5, 280, 'Ivrea', 'La joven Miaka y su amiga Yugi viajan al mundo m&aacute;gico a causa de la lectura de un misterioso libro. All&iacute;, un apuesto joven llamado Tamahome las salva de unos delincuentes. Sin embargo, Yugi desaparece mientras Miaka descubre que es la sacerdotisa de Suzaku, la &uacute;nica persona capaz de traer la paz al reino de Kohnan gracias a la ayuda de siete compa&ntilde;eros con poderes m&aacute;gicos. Lo que no sabe nuestra protagonista es que para conseguir su prop&oacute;sito deber&aacute; enfrentarse a su amiga de la infancia: Yui, quien ha sido coronada como la sacerdotisa de Seiryu, clan rival de Suzaku. Empieza una aventura m&aacute;gica inolvidable...', 'fushigit2.jpg'),
(15, 'Fushigi Yuugi Tomo 3', 'Yuu Watase', 'Shoujo', 5, 280, 'Ivrea', 'La joven Miaka y su amiga Yugi viajan al mundo m&aacute;gico a causa de la lectura de un misterioso libro. All&iacute;, un apuesto joven llamado Tamahome las salva de unos delincuentes. Sin embargo, Yugi desaparece mientras Miaka descubre que es la sacerdotisa de Suzaku, la &uacute;nica persona capaz de traer la paz al reino de Kohnan gracias a la ayuda de siete compa&ntilde;eros con poderes m&aacute;gicos. Lo que no sabe nuestra protagonista es que para conseguir su prop&oacute;sito deber&aacute; enfrentarse a su amiga de la infancia: Yui, quien ha sido coronada como la sacerdotisa de Seiryu, clan rival de Suzaku. Empieza una aventura m&aacute;gica inolvidable...', 'fushigit3.jpg'),
(16, 'Nana Tomo 1', 'Ai Yazawa', 'Josei', 4, 280, 'Ivrea', 'Esta es la historia de dos chicas de mismo nombre, pero muy diferentes entre s&iacute;. Nana Komatsu es una chica enamoradiza y supersticiosa; piensa que su nombre (siete, en japon&eacute;s) le trae mala suerte. Despu&eacute;s de una larga historia de amores y desamores, finalmente encuentra al que cree que ser&aacute; el chico de su vida, Shouji Endo, pero &eacute;ste se va a Tokio a estudiar para los ex&aacute;menes de acceso. Nana Oosaki, por el contrario, es una chica de caracter fuerte e independiente; es cantante de una banda punk llamada Black Stones (o Blast), y tiene una relaci&oacute;n con el bajista de dicha banda, Ren Honjo. Sin embargo, Ren decide abandonar la banda y a Nana para irse a Tokio con otra banda, Trapnest. Ambas Nanas, por causa del destino, se conocer&aacute;n en el viaje de tren destino a Tokio (que ambas emprenden por distintas causas); m&aacute;s tarde se convertir&aacute;n en compa&ntilde;eras de piso y compartir&aacute;n muchas vivencias, tanto alegres como tristes, en las que se tendr&aacute;n la una a la otra.', 'nanat1.jpg'),
(17, 'Nana Tomo 2', 'Ai Yazawa', 'Josei', 4, 280, 'Ivrea', 'Esta es la historia de dos chicas de mismo nombre, pero muy diferentes entre s&iacute;. Nana Komatsu es una chica enamoradiza y supersticiosa; piensa que su nombre (siete, en japon&eacute;s) le trae mala suerte. Despu&eacute;s de una larga historia de amores y desamores, finalmente encuentra al que cree que ser&aacute; el chico de su vida, Shouji Endo, pero &eacute;ste se va a Tokio a estudiar para los ex&aacute;menes de acceso. Nana Oosaki, por el contrario, es una chica de caracter fuerte e independiente; es cantante de una banda punk llamada Black Stones (o Blast), y tiene una relaci&oacute;n con el bajista de dicha banda, Ren Honjo. Sin embargo, Ren decide abandonar la banda y a Nana para irse a Tokio con otra banda, Trapnest. Ambas Nanas, por causa del destino, se conocer&aacute;n en el viaje de tren destino a Tokio (que ambas emprenden por distintas causas); m&aacute;s tarde se convertir&aacute;n en compa&ntilde;eras de piso y compartir&aacute;n muchas vivencias, tanto alegres como tristes, en las que se tendr&aacute;n la una a la otra.', 'nanat2.jpg'),
(18, 'Nana Tomo 3', 'Ai Yazawa', 'Josei', 4, 280, 'Ivrea', 'Esta es la historia de dos chicas de mismo nombre, pero muy diferentes entre s&iacute;. Nana Komatsu es una chica enamoradiza y supersticiosa; piensa que su nombre (siete, en japon&eacute;s) le trae mala suerte. Despu&eacute;s de una larga historia de amores y desamores, finalmente encuentra al que cree que ser&aacute; el chico de su vida, Shouji Endo, pero &eacute;ste se va a Tokio a estudiar para los ex&aacute;menes de acceso. Nana Oosaki, por el contrario, es una chica de caracter fuerte e independiente; es cantante de una banda punk llamada Black Stones (o Blast), y tiene una relaci&oacute;n con el bajista de dicha banda, Ren Honjo. Sin embargo, Ren decide abandonar la banda y a Nana para irse a Tokio con otra banda, Trapnest. Ambas Nanas, por causa del destino, se conocer&aacute;n en el viaje de tren destino a Tokio (que ambas emprenden por distintas causas); m&aacute;s tarde se convertir&aacute;n en compa&ntilde;eras de piso y compartir&aacute;n muchas vivencias, tanto alegres como tristes, en las que se tendr&aacute;n la una a la otra.', 'nanat3.jpg'),
(19, 'Dr. Slump Tomo 1', 'Akira Toriyama', 'Kodomo', 3, 780, 'DeAgostini', 'Dr. Slump se encuentra en Penguin Village, un lugar donde coexisten con todo tipo de animales anthropormorphic y otros objetos de los seres humanos. En este pueblo vive Norimaki Senbei, un inventor (su nombre es un juego de palabras con una especie de galleta de arroz). Su apodo es &quot;Dr. Slump&quot; (una broma que se puede ver como algo similar a apodar un autor &quot;bloqueo del escritor&quot;.) En el primer n&uacute;mero, que construye una ni&ntilde;a robot nombra Norimaki Arale (un juego de palabras con otro tipo de arroz galleta), en las escenas, obviamente, parodiando cl&aacute;sico Pinocho de los ni&ntilde;os italianos. Debido Senbei es un inventor p&eacute;simo, pronto resulta ser en grave necesidad de anteojos. Ella tambi&eacute;n es muy ingenuo, y en ediciones posteriores ella tiene aventuras tales como traer un enorme oso de la casa, despu&eacute;s de haber confundido con un animal de compa&ntilde;&iacute;a. A favor de Senbei, ella tiene s&uacute;per fuerza.', 'drslumpt1.jpg'),
(20, 'Dr. Slump Tomo 2', 'Akira Toriyama', 'Kodomo', 3, 780, 'DeAgostini', 'Dr. Slump se encuentra en Penguin Village, un lugar donde coexisten con todo tipo de animales anthropormorphic y otros objetos de los seres humanos. En este pueblo vive Norimaki Senbei, un inventor (su nombre es un juego de palabras con una especie de galleta de arroz). Su apodo es &quot;Dr. Slump&quot; (una broma que se puede ver como algo similar a apodar un autor &quot;bloqueo del escritor&quot;.) En el primer n&uacute;mero, que construye una ni&ntilde;a robot nombra Norimaki Arale (un juego de palabras con otro tipo de arroz galleta), en las escenas, obviamente, parodiando cl&aacute;sico Pinocho de los ni&ntilde;os italianos. Debido Senbei es un inventor p&eacute;simo, pronto resulta ser en grave necesidad de anteojos. Ella tambi&eacute;n es muy ingenuo, y en ediciones posteriores ella tiene aventuras tales como traer un enorme oso de la casa, despu&eacute;s de haber confundido con un animal de compa&ntilde;&iacute;a. A favor de Senbei, ella tiene s&uacute;per fuerza.', 'drslumpt2.jpg'),
(21, 'Dr. Slump Tomo 3', 'Akira Toriyama', 'Kodomo', 3, 780, 'DeAgostini', 'Dr. Slump se encuentra en Penguin Village, un lugar donde coexisten con todo tipo de animales anthropormorphic y otros objetos de los seres humanos. En este pueblo vive Norimaki Senbei, un inventor (su nombre es un juego de palabras con una especie de galleta de arroz). Su apodo es &quot;Dr. Slump&quot; (una broma que se puede ver como algo similar a apodar un autor &quot;bloqueo del escritor&quot;.) En el primer n&uacute;mero, que construye una ni&ntilde;a robot nombra Norimaki Arale (un juego de palabras con otro tipo de arroz galleta), en las escenas, obviamente, parodiando cl&aacute;sico Pinocho de los ni&ntilde;os italianos. Debido Senbei es un inventor p&eacute;simo, pronto resulta ser en grave necesidad de anteojos. Ella tambi&eacute;n es muy ingenuo, y en ediciones posteriores ella tiene aventuras tales como traer un enorme oso de la casa, despu&eacute;s de haber confundido con un animal de compa&ntilde;&iacute;a. A favor de Senbei, ella tiene s&uacute;per fuerza.', 'drslumpt3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mangageneros`
--

CREATE TABLE `mangageneros` (
  `id` int(11) NOT NULL,
  `Nombre` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mangageneros`
--

INSERT INTO `mangageneros` (`id`, `Nombre`) VALUES
(1, 'Seinen'),
(2, 'Shounen'),
(3, 'Kodomo'),
(4, 'Josei'),
(5, 'Shoujo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodxventas`
--

CREATE TABLE `prodxventas` (
  `id_pxv` int(11) NOT NULL,
  `id_venta` date NOT NULL,
  `id_prod` int(11) NOT NULL,
  `precio_u` int(11) NOT NULL,
  `cant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `email` text NOT NULL,
  `estado` int(1) NOT NULL,
  `ultima_conexion` date NOT NULL,
  `recuperar` int(11) NOT NULL,
  `verificación` int(11) NOT NULL,
  `pass` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `email`, `estado`, `ultima_conexion`, `recuperar`, `verificación`, `pass`) VALUES
(2, 'francio', 'franco.delocca@gmail.com', 1, '2019-09-01', 705879695, 0, '6a2133f82baf2faa18af883ca70f947ee1e8d31f9a3e7492a1e83523ecff1fa8f296efc959dcdd2a1f336077d5f48e66fbc1d1756c36bec2746dd43858b551a6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_ventas` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `total` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mangacatalogo`
--
ALTER TABLE `mangacatalogo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mangageneros`
--
ALTER TABLE `mangageneros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prodxventas`
--
ALTER TABLE `prodxventas`
  ADD PRIMARY KEY (`id_pxv`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_ventas`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mangacatalogo`
--
ALTER TABLE `mangacatalogo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `mangageneros`
--
ALTER TABLE `mangageneros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `prodxventas`
--
ALTER TABLE `prodxventas`
  MODIFY `id_pxv` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_ventas` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
