<div class="menu_bar_m">
			<a href="#" class="bt-menu_m"><span class="lnr lnr-menu"></span>Menu</a>
		</div>
 
		<nav class="nav_m">
			<ul>
				<li><a href="index.php"><span class="lnr lnr-home"></span>Inicio</a></li>
				<li><a href="catalogo.php"><span class="lnr lnr-book"></span>Catalogo</a></li>
				<li><a href="contacto.php"><span class="lnr lnr-envelope"></span>Contacto</a></li>
				

				<?php 
					if (!isset($_SESSION['usuario'])) {
						echo '<div class="boxFloat">
						<li><a href="login/login.php">
				<i class="far fa-user" height="50px" width="50px" style="margin-right: 10px;"></i><span>Iniciar Sesión</span>
				</a></li>
				<li><a href="login/registrate.php">
				<i class="far fa-edit" height="50px" width="50px" style="margin-right: 10px;"></i><span>Registrarse</span>
				</a></li>
				</div>';
					}else{
						echo '<li><a href="login/cerrar.php"><i class="far fa-user-slash"></i><span>Cerrar Sesión</span></a></li>';
					}
				 ?>
				
				<div class="filtroMobile">
				<form action="catalogo.php" class="buscar">
			<input type="text" name="buscar" placeholder="Buscar...">
			<button type="submit" name="lupa" value=""><i class="fas fa-search"></i></button>
		</form>
   </div>
			</ul>
		</nav>
<script src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>