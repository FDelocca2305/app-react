<?php

function agregarPrimerProducto($id)
{
	include("includes/conexion.php");
	$sql = "SELECT * FROM mangacatalogo WHERE id=" . $id . "";
	$resultado = $conexion->query($sql);
	while ($registro = $resultado->fetch_array()) {

		$id_prod = $registro['id'];
		$nombre = $registro['nombre'];
		$editorial = $registro['editorial'];
		$autor = $registro['autor'];
		$imagen = $registro['portada'];
		$Precio = $registro['precio'];
		$Cantidad = 1; //por ser la primera vez que cargo el producto
	}
	$prods_compra[] = array(
		'id' => $id_prod, 'nombre' => $nombre, 'editorial' => $editorial, 'autor' => $autor,
		'precio' => $Precio, 'portada' => $imagen, 'cantidad' => $Cantidad
	);
	//CREAMOS LA VARIABLE DE SESSION $_SESSION['carrito']
	$_SESSION['carrito'] = $prods_compra;
	$resultado->free();
	$conexion->close();
}
function mostrarProductosCarrito()
{
	//a veces llamamos a la funcion y el carrito ya no existe por ejemplo porque
	// eliminamos el ultimo producto por lo cual eliminamos la variable de sesion carrito
	if (!isset($_SESSION['carrito'])) {
		echo '<section class="containerCart">
        <section class="sectionProducts">
            <h1 style="font-size: 35px">TU CARRITO</h1>
            <hr>
            <div>';
		echo "carrito vacio ";
		echo '</div>
        </section>
        <section class="sectionResume">
            <p>RESUMEN DE COMPRA</p>
            <!-- DENTRO DE LOS SPAN VAN LA SUMA DE LOS PRECIOS  -->
            <div class="divResume">
                <div class="boxValues">
                    <div>
                        <div class="valueText">
                            <p style="font-weight:bold">SUBTOTAL </p><span style="font-weight:100">...... $0.00</span>
                        </div>
                    </div>
                    <div>
                        <div class="valueText">

                            <p style="font-weight:bold">TOTAL(IVA) </p>

                            <span style="font-weight:100">...... $0.00</span>

                        </div>
                    </div>
                </div>
                <div>
                    <div class="botoncomprar">
                        <button class="btn_comprar" id="boton">CONFIRMAR COMPRA</button>
                    </div>
                </div>
            </div>
        </section>
    </section>';
	} else {
		$total = 0;
		$prods_compra = $_SESSION['carrito'];
		echo '<section class="containerCart">
        <section class="sectionProducts">
            <h1 style="font-size: 35px">TU CARRITO</h1>
            <hr>
            <div>';
		foreach ($prods_compra as  $indice => $producto) {
			echo '<div class="productCart">
                        <div>
                            <img src="images/' . $producto['portada'] . '" alt="">
                        </div>
                        <div class="productDetails">
                            <div>
                                Titulo: ' . $producto['nombre'] . '
                            </div>
                            <div>
                                Editorial:' . $producto['editorial'] . '
                            </div>
                            <div>
                                Autor: ' . $producto['autor'] . '
                            </div>
                        </div>
                        <div>
						   <input type="number" name="quantity" min="1" max="99" placeholder="' . $producto['cantidad'] . '" value="' . $producto['cantidad'] . '"><!-- INPUT QUE AUMENTA EL NUMERO DE ARTICULOS -->
						   ';
			if ($prods_compra[$indice]['cantidad'] > 1) {
				echo '<a href="carrito.php?id_resta=' . $prods_compra[$indice]['id'] . '" style="margin-left:10px; margin-right:10px"> <i class="fas fa-minus"></i> </a> ';
				echo '|';
				echo '<a href="carrito.php?id_suma=' . $prods_compra[$indice]['id'] . '" style="margin-left:10px"> <i class="fas fa-plus"></i> </a><br> ';
			} else {
				echo '<a href="carrito.php?id_suma=' . $prods_compra[$indice]['id'] . '">
				+ </a><br> ';
			}
			echo '
                        </div>
                        <div>
                            $ ' . $producto['precio'] . ' <!-- PRECIO DE LOS ARTICULOS UNITARIO -->
                        </div>
                        <div>
                        <a href="carrito.php?id_borra=' . $prods_compra[$indice]['id'] . '"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                        </div>
                    </div>';


			$total = $total + ($prods_compra[$indice]['cantidad'] * $prods_compra[$indice]['precio']);
		}
		echo '</div>
        </section>
        <section class="sectionResume">
            <p>RESUMEN DE COMPRA</p>
            <!-- DENTRO DE LOS SPAN VAN LA SUMA DE LOS PRECIOS  -->
            <div class="divResume">
                <div class="boxValues">
                    <div>
                        <div class="valueText">
                            <p style="font-weight:bold">SUBTOTAL </p><span style="font-weight:100">...... $' . $total . '</span>
                        </div>
                    </div>
                    <div>
                        <div class="valueText">

                            <p style="font-weight:bold">TOTAL(IVA) </p>

                            <span style="font-weight:100">...... $' . ($total + ($total * 0.21)) . '</span>

                        </div>
                    </div>
                </div>
                <div>
                    <div class="botoncomprar">
                        <a href="comprar.php"><button class="btn_comprar" id="boton">CONFIRMAR COMPRA</button></a>
                    </div>
                </div>
            </div>
        </section>
    </section>';
	}
}
function buscarSiProductoExite($id)
{
	$prods_compra = $_SESSION['carrito'];
	$existe = 0;
	foreach ($prods_compra as $indice => $producto) {
		if ($producto['id'] == $id) {
			$existe = 1;
			$prods_compra[$indice]['cantidad']++;
		}
	}
	$_SESSION['carrito'] = $prods_compra;
	return $existe;
}
function agregarNuevoProducto($id)
{
	$prods_compra = $_SESSION['carrito'];
	include("includes/conexion.php");
	$sql = "SELECT * FROM mangacatalogo WHERE id=" . $id . "";
	$resultado = $conexion->query($sql);
	while ($registro = $resultado->fetch_array()) {

		$id_prod = $registro['id'];
		$nombre = $registro['nombre'];
		$editorial = $registro['editorial'];
		$autor = $registro['autor'];
		$imagen = $registro['portada'];
		$Precio = $registro['precio'];
		$Cantidad = 1; //por ser la primera vez que cargo el producto
	}
	$nuevo_prod = array(
		'id' => $id_prod, 'nombre' => $nombre, 'editorial' => $editorial, 'autor' => $autor,
		'precio' => $Precio, 'portada' => $imagen, 'cantidad' => $Cantidad
	);
	array_push($prods_compra, $nuevo_prod);
	$_SESSION['carrito'] = $prods_compra;
	$resultado->free();
	$conexion->close();
}
function sumarCantidad($id)
{
	$prods_compra = $_SESSION['carrito'];
	foreach ($prods_compra as $indice => $producto) {
		if ($producto['id'] == $id) {
			$prods_compra[$indice]['cantidad']++;
		}
	}
	$_SESSION['carrito'] = $prods_compra;
}
function restarCantidad($id)
{
	$prods_compra = $_SESSION['carrito'];
	foreach ($prods_compra as $indice => $producto) {
		if ($producto['id'] == $id) {
			$prods_compra[$indice]['cantidad']--;
		}
	}
	$_SESSION['carrito'] = $prods_compra;
}

function eliminarProdCarrito($id)
{
	$prods_compra = $_SESSION['carrito'];
	foreach ($prods_compra as $indice => $producto) {
		if ($producto['id'] == $id) {
			unset($prods_compra[$indice]);
		}
	}
	// hay que fijarse si el carrito esta vacio 
	//eliminar la variable de sesion carrito
	if (count($prods_compra) > 0) {
		$_SESSION['carrito'] = $prods_compra;
	} else {
		unset($_SESSION['carrito']);
	}
}

/* function confirmarCompra()
{
	$prods_compra = $_SESSION['carrito'];
	$total = 0;
	foreach ($prods_compra as  $indice => $producto) {
		echo 'id - ' . $producto['id'] . '<br>';
		echo 'nombre - ' . $producto['nombre'] . '<br>';
		echo 'precio - ' . $producto['precio'] . '<br>';
		echo 'cantidad - ' . $producto['cantidad'] . '<br>';
		echo 'Subtotal: $' . $prods_compra[$indice]['cantidad'] * $prods_compra[$indice]['precio'] . '<br><br>';

		$total = $total + ($prods_compra[$indice]['cantidad'] * $prods_compra[$indice]['precio']);
	}
	echo 'TOTAL COMPRA $' . $total . '<br><br>';
} */

function comprar()
{

	include("includes/conexion.php");
	$fecha = date("Y-m-d");
	$usuario = 1;
	$sql = "INSERT INTO ventas (fecha, id_usuario) VALUES ('$fecha','$usuario')";
	$insert = $conexion->query($sql);


	$sqlc = "SELECT id_ventas FROM ventas ORDER BY id_ventas desc LIMIT 1,1";
	$consulta = $conexion->query($sqlc);
	$registro = $consulta->fetch_array();
	
	$id_venta = $registro[0];

	$prods_compra = $_SESSION['carrito'];
	$total = 0;
	foreach ($prods_compra as $indice => $producto) {
		$id_prod = $producto['id'];
		$precio = $producto['precio'];
		$cantidad = $producto['cantidad'];

		$sqli = "INSERT INTO prodxventas (id_venta, id_prod, precio_u, cant) VALUES ('$registro[0]','$id_prod','$precio','$cantidad')";
		$insertar = $conexion->query($sqli) ? print("ok") : print("Ups, :(");

		$total = $total + ($prods_compra[$indice]['precio'] * $prods_compra[$indice]['cantidad']);
	}

	$total= $total+($total*0.21);
	$sql = "UPDATE ventas SET total='$total'";
	$actualizar = $conexion->query($sql) ? print("ok") : print("Ups, :(");




}
