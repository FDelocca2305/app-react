<?php session_start(); //muy importante para las sesiones, debe estar en todas las páginas

/* if (isset($_GET['verificar'])) {
	$verificacion = $_GET['verificar'];
	$sql = "UPDATE usuarios SET estado = 1 WHERE verificacion='$verificacion'";
	$consulta2 = mysqli_query($conexion, $sql);
	header('Location: login/login.php');
}

if (isset($_SESSION['usuario'])) {

	$usuariover = $_SESSION['usuario'];

	$sql = "SELECT estado FROM usuarios WHERE usuario = '$usuariover' LIMIT 1";
	$consulta = mysqli_query($conexion, $sql);
	if (mysqli_num_rows($consulta) > 0) {

		while ($registro = mysqli_fetch_assoc($consulta)) {

			if ($registro['estado'] == 0) {

				header('Location: login/views/verificar.php');
			}
		}
	}
} else {
	if (isset($_GET['codigo'])) {
		header('Location: login/views/recuperar_reset_view.php');
	} else {
		header('Location: login/registrate.php');
	}
	//si tiene una sesión lo envía al contenido, si no a registrarse
} */
?>

<!DOCTYPE html>
<html>

<head>
	<title>Inicio | Portal Comics</title>
	<link rel="icon" type="image/png" href="/images/icons/LogoPestaña.png" />
	<link rel="stylesheet" type="text/css" href="css/slider.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/header.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link rel="stylesheet" type="text/css" href="css/whatsapp.css">
	<link rel="stylesheet" type="text/css" href="css/productosReco.css">
	<link rel="stylesheet" type="text/css" href="css/indexProductos.css">
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/png" href="images/icons/LogoPestaña.png"/>
	<link rel="stylesheet" type="text/css" href="fonts/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>

<body>

	<header>
		<?php require("includes/header.php") ?>
		<?php if (isset($_GET['buscar'])) {
			$buscar = $_GET['buscar'];
			header('Location: catalogo.php?');
		}
		?>
	</header>

	<?php require("includes/menu.php") ?>
	<?php require("includes/slider.html") ?>
	<?php require("includes/indexReco.php") ?>
	<?php require("includes/indexProductos.php") ?>

	<?php require("includes/whatsapp.php") ?>
	<?php require("includes/footer.html") ?>
	<script type="text/javascript" src="js/footer.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
</body>

</html>