<?php
require("includes/conexion.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $nombre = $_POST['nombre'];
    $email = $_POST['email'];
	$telefono = $_POST['telefono'];
    $mensaje = $_POST['mensaje'];

       
        $to  = 'franco.delocca@gmail.com';
        $subject   = 'Consulta';
        $message   = 'Recibiste una consulta de: '.$nombre.' Telefono: '.$telefono.' Mensaje: '.$mensaje;
        $cabeceras = 'From:'.$email;

        mail($to, $subject,$message, $cabeceras);
	}
	echo"<script>
	$( '.btn_comprar' ).click(function() {
		Swal.fire({
		  type: 'success',
		  title: 'Se envio tu consulta',
		  text: 'Recibiremos un mail',
		  footer: 'Te enviaremos un feedback cuando podamos'
	})
	});
</script>";
?>

<!DOCTYPE html>
<html>

<head>
	<title>Contacto | Portal Comics</title>
	<link rel="stylesheet" type="text/css" href="css/slider.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="icon" type="image/png" href="/images/icons/LogoPestaña.png" />
	<link rel="stylesheet" type="text/css" href="css/header.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link rel="stylesheet" type="text/css" href="css/whatsapp.css">
	<link rel="stylesheet" type="text/css" href="css/productosReco.css">
	<link rel="stylesheet" type="text/css" href="css/indexProductos.css">
    <link rel="stylesheet" type="text/css" href="css/contacto.css">
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/png" href="images/icons/LogoPestaña.png"/>
	<link rel="stylesheet" type="text/css" href="fonts/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>

<body>

	<header>
		<?php require("includes/header.php") ?>
		<?php if (isset($_GET['buscar'])) {
			$buscar = $_GET['buscar'];
			header('Location: catalogo.php?');
		}
		?>
	</header>

	<?php require("includes/menu.php") ?>
        <section>

            <div class="elemento">
                <form action="" class="formulario" method="POST">     
                    <div class="form-group">
                        <span class="lnr lnr-user" id="icono"></span><input type="text" id="nombre" name="nombre" placeholder="Nombre y Apellido">
                    </div>
                    <div class="form-group">
                        <span class="lnr lnr-envelope" id="icono"></span><input type="email" id="email" name="email" placeholder="Email">
					</div>
					<div class="form-group">
                        <span class="lnr lnr-phone" id="icono"></span><input type="text" id="email" name="telefono" placeholder="Telefono">
					</div>
					<div class="form-group">
                        <textarea class="contactText" name="mensaje" cols="62" rows="10" placeholder="Escribir mensaje..."></textarea>
					</div>
                    <div class="botoncomprar">
                        <button class="btn_comprar" id="formulario">Consultar</button>
                    </div>
                </form>
			</div>
			
            <div class="elemento">
            	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d13124.880548547897!2d-58.33746854999998!3d-34.67439315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sar!4v1569943182598!5m2!1ses-419!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
			
        </section>
		<?php require("includes/whatsapp.php")?>
		<?php require("includes/footer.html") ?>
	<script type="text/javascript" src="js/footer.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
</body>

</html>