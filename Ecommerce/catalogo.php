<?php
    require_once("includes/conexion.php")
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <!---->
    
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <!---->
    <link rel="stylesheet" type="text/css" href="css/slider.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/header.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="icon" type="image/png" href="/images/icons/LogoPestaña.png" />
	<link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/whatsapp.css">
    <link rel="stylesheet" type="text/css" href="css/productosReco.css">
    <link rel="stylesheet" type="text/css" href="css/filtrosM.css">
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/png" href="images/icons/LogoPestaña.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css"  href="css/catalogo.css">
    <title>Portal Cómics</title>
</head>
<body>
<header>
<?php require("includes/header.php") ?>

</header>


    <!---->
    <?php require("includes/menu.php");
    include("includes/filtroM.php"); ?>
    <!---->

<div class="contenedor">
      
   
   <?php
        include("includes/filtro.php");
        include("includes/listaProductos.php");
    ?>
</div>

    <?php include("includes/cpag.php");?>

    <?php require("includes/whatsapp.php");?>
    <?php include("includes/footer.html");?>
    <script type="text/javascript" src="js/footer.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
</body>
</html>