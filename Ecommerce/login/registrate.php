<?php session_start();
include("conexion.php");
if (isset($_SESSION['usuario'])) {
    header('Location: index.php');
}

//Esto es para que si ya tiene una sesión, no intente crear otra. No puede acceder a los formularios.

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING);
    $password = $_POST['password'];
    $password2 = $_POST['password2'];
    $email = $_POST['email'];
    $estado = 0;
    $ultima_conexion = date('Y-m-d');
    $recuperar = 0;

    $errores = '';

    if (empty($usuario) or empty($password) or empty($password2) or empty($email)) {
        $errores .= '<li>Error, completá el formulario correctamente.</li>';
    } else {

        $sql = "SELECT * FROM usuarios WHERE usuario = '$usuario' LIMIT 1";
        $consulta = mysqli_query($conexion, $sql);


        if (mysqli_num_rows($consulta) > 0) {

            while ($registro = mysqli_fetch_assoc($consulta)) {


                if ($registro['usuario'] == $usuario) {
                    $errores .= '<li>El nombre de usuario ya existe.</li>';
                }
            }
        }
        $password = hash('sha512', $password);
        $password2 = hash('sha512', $password2);
        //"Encripta" las contraseñas.

        if ($password != $password2) {
            $errores .= '<li>Las contraseñas no son iguales</li>';
        }

        $verificacion = rand(999999, 999999999);

        $sql2 = "INSERT INTO usuarios (usuario,email,estado,ultima_conexion,recuperar,verificacion,pass) VALUES ('$usuario', '$email', $estado,'$ultima_conexion',$recuperar, $verificacion, '$password')";
        $consulta = mysqli_query($conexion,$sql2);



        $link = "https://portalcomics.000webhostapp.com/index.php?verificar=" . $verificacion;

        $para      = $email;
        $titulo    = 'Verificación de Cuenta';
        $mensaje   = 'Haz click en el link para verificar tu cuenta ' . $link;
        $cabeceras = 'portal_comics@gmail.com';

        mail($para, $titulo, $mensaje, $cabeceras);

        header('Location: views/verificar.view.php');
    }
}

//Comprueba que los datos se enviaron (del echo).
//Entonces los guarda.
//strwolower saca las mayúsculas.
//Las otras funciones sirven para añadir seguridad (no se pueda inyectar código).
//$errores sirve para ver los errores; si está vacía todo salió bien.
//try/catch para conectar a la BD.
//:usuario es igual al del placeholder.
include("views/registrate.php");
?>