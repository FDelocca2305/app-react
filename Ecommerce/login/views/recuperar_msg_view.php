<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
    <title>Recuperar contraseña</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fff83f4516.js"></script>
    <link rel="stylesheet" href="../css/estilos.css">
</head>
<body>
    <div class="contenedor">
    <h1 class="titulo">Recuperar contraseña</h1>
        <hr class="border">

        <form action="" method="POST" class="formulario" name="recuperar">
        
        <div class="form-group">
                <h2>
                Se ha enviado un Email al correo ingresado para recuperar su contraseña
                </h2>
            </div>
           
        </form>
    </div>
</body>
</html>