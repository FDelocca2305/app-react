<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Iniciar sesión</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fff83f4516.js"></script>
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>
    <div class="contenedor">
        <h1 class="titulo">Iniciar sesión</h1>
        <hr class="border">
        
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" class="formulario" name="login">
            <div class="form-group">
                <i class="icono izquierda fas fa-user"></i><input type="text" name="usuario" class="usuario" placeholder="Usuario" value="<?php if(isset($_COOKIE['usuario'])) { echo $_COOKIE['usuario']; } ?>">
            </div>
            
            <div class="form-group">
                <i class="icono izquierda fas fa-lock"></i><input type="password" name="password" class="password_btn" placeholder="Contraseña" value="<?php if(isset($_COOKIE['password'])) { echo $_COOKIE['password']; } ?>">
                <i class="submit-btn fas fa-arrow-right" onclick="login.submit()"></i>
            </div>
            <div class="form-check">
          <input type="checkbox" name="remember" id="remember" <?php if(isset($_COOKIE["usuario"])) { ?> checked
            <?php } ?>  class="form-check-input"/>
          <label for="remember-me">Recordarme</label>
          <p class="texto-registrate">
        <a href="views/recuperar_mail_view.php"> ¿Olvidaste tu contraseña?</a>
        </p>
        </div>
            <?php if(!empty($errores)): ?>
                <div class="error">
                    <ul>
                        <?php echo $errores; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </form>
        
        <p class="texto-registrate">
            ¿Aún no estás registrado?
            <a href="registrate.php">Registrarse</a>
        </p>
        
    </div>
</body>
</html>