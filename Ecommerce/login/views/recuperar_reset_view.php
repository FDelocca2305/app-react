<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
    <title>Recuperar contraseña</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fff83f4516.js"></script>
    <link rel="stylesheet" href="../css/estilos.css">
</head>
<body>
    <div class="contenedor">
    <h1 class="titulo">Recuperar contraseña</h1>
        <hr class="border">

        <form action="recuperar.php" method="POST" class="formulario" name="recuperar">
            <div class="form-group">
                <i class="icono izquierda fas fa-user"></i><input type="text" name="usuario" class="usuario" placeholder="Usuario">
            </div>
            
            <div class="form-group">
                <i class="icono izquierda fas fa-lock"></i><input type="password" name="password" class="password_btn" placeholder="Nueva Contraseña" >           
            </div>

            <div class="form-group">
                <i class="icono izquierda fas fa-lock"></i><input type="password" name="password2" class="password_btn" placeholder="Repetir Contraseña">
            </div>

            <input type="submit" value="Enviar">
        </form>
    </div>
</body>
</html>