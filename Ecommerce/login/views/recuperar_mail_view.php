<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
    <title>Recuperar contraseña</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fff83f4516.js"></script>
    <link rel="stylesheet" href="../css/estilos.css">
</head>
<body>
    <div class="contenedor">
    <h1 class="titulo">Recuperar contraseña</h1>
        <hr class="border">

        <form action="recuperar.php" method="POST" class="formulario" name="recuperar">
        
        <div class="form-group">
                <i class="icono izquierda fas fa-envelope"></i><input type="email" name="email" class="email" placeholder="Correo electrónico">
            </div>
            <input type="submit" value="Enviar">
        </form>
    </div>
</body>
</html>