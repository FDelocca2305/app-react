<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Contenido</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fff83f4516.js"></script>
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>
    <div class="contenedor">
        <h1 class="titulo">
            Contenido del sitio
        </h1>
        <a href="cerrar.php">Cerrar sesión</a>
        <hr class="border">
        <div class="contenido">
           <article>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit non incidunt, laborum repellat facilis possimus mollitia fugiat accusamus ratione architecto quis sit saepe commodi natus vel ipsa nulla consequatur, voluptates illum quisquam maiores perspiciatis. Molestias laboriosam dignissimos suscipit fuga at accusantium mollitia aut perspiciatis magnam tempora in atque nobis, eligendi!</p>
            
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolorum nesciunt quis consectetur, at tempora aperiam fugiat harum qui sequi magnam voluptatem in, autem, excepturi. Est ratione, obcaecati atque officiis impedit a architecto molestias consectetur, explicabo ipsa esse consequuntur! Exercitationem quaerat, consequuntur reprehenderit aliquid deleniti ab suscipit voluptatibus nemo dolore?</p>
            </article>
        </div>
    </div>
</body>
</html>