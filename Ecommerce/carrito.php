<?php
session_start();
include('includes/carrito/libreria.php') ?>


<!DOCTYPE html>
<html>

<head>
    <title>Carrito | Portal Comics</title>
    <link rel="stylesheet" type="text/css" href="css/slider.css">
    <link rel="icon" type="image/png" href="/images/icons/LogoPestaña.png" />
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/whatsapp.css">
    <link rel="stylesheet" type="text/css" href="css/carrito.css">
    <link rel="stylesheet" type="text/css" href="css/productosReco.css">
    <link rel="stylesheet" type="text/css" href="css/indexProductos.css">
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/png" href="images/icons/LogoPestaña.png"/>
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>

<body>

    <header>
        <?php require("includes/header.php") ?>
        <?php if (isset($_GET['buscar'])) {
            $buscar = $_GET['buscar'];
            header('Location: catalogo.php?');
        }
        ?>
    </header>

    <?php require("includes/menu.php") ?>

    <?php if (isset($_SESSION['carrito'])) {
        if (isset($_GET['id'])) {
            //ingresa un nuevo producto o un producto existente para actualizar cantidad
            $existe = buscarSiProductoExite($_GET['id']);
            if ($existe == 0) {
                //ingresa un nuevo producto
                agregarNuevoProducto($_GET['id']);
            }
        }
        if (isset($_GET['id_suma'])) {
            sumarCantidad($_GET['id_suma']);
        }
        if (isset($_GET['id_resta'])) {
            restarCantidad($_GET['id_resta']);
        }
        if (isset($_GET['id_borra'])) {
            eliminarProdCarrito($_GET['id_borra']);
        }
        mostrarProductosCarrito();
        if (isset($_SESSION['carrito'])) {
            /* echo '<a href="comprar.php">Finalizar compra</a><br>'; */ }
    } elseif (isset($_GET['id'])) {
        // COMO NO EXISTE $_SESSION['carrito']
        //quiere decir que ingresa el primer producto al carrito
        agregarPrimerProducto($_GET['id']);
        mostrarProductosCarrito();
        /*  echo '<a href="comprar.php">Finalizar compra</a><br>'; */
    } else {
        ?>

        <div class="emptyCart">
            <div>
                <img src="images/carrito_vacio.png">
            </div>
            <div>
                <h1>Tu carrito se encuentra vacío.</h1>
            </div>
        </div>
        
        <?php  


            }
    /* echo '<a href="./">Seguir viendo productos</a>'; */
    ?>


    <?php require("includes/whatsapp.php") ?>
    <?php require("includes/footer.html") ?>
    <script type="text/javascript" src="js/footer.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>

</body>

</html>