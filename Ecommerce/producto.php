<?php
require_once("includes/conexion.php")
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <!---->
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <!---->
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="favicon" type="image/png" href="/images/icons/LogoPestaña.png" />
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/indexProductos.css">
    <link rel="stylesheet" type="text/css" href="css/producto.css">
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/png" href="images/icons/LogoPestaña.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/catalogo.css">
    <title>Portal Cómics</title>
</head>

<body>
    <header>
        <?php require("includes/header.php");
        if (isset($_GET['buscar'])) {
            $buscar = $_GET['buscar'];
            header('Location: catalogo.php?');
        }

        ?>


    </header>

    <!---->
    <?php require("includes/menu.php") ?>
    <!---->
    <?php

    $varProducto = $_GET["producto"];

    $sql1 = "SELECT * FROM mangacatalogo WHERE id = " . $varProducto;

    $consulta1 = mysqli_query($conexion, $sql1);

    ?>

    <div class="containerp">

        <?php

        if (mysqli_num_rows($consulta1) > 0) {


            while ($registro1 = mysqli_fetch_assoc($consulta1)) {

                ?>

                <div class="cajadetalles">

                    <div class="imagen">
                        <img src="images/<?php echo $registro1['portada'] ?>" /></div>
                    <div class="informacion">
                        <h1><?php echo $registro1['nombre'] ?></h1>
                        <h3>Autor: <?php echo $registro1['autor'] ?></h3>
                        <h3>Género: <?php echo $registro1['genero'] ?></h3>
                        <h3>Editorial: <?php echo $registro1['editorial'] ?></h3>
                        <h2>$<?php echo $registro1['precio'] ?></h2>

                    </div>
                    <div class="whitespace_3"></div>
                    <div class="botoncomprar">
                        <a class="a_ag" href="carrito.php?id=<?php echo $registro1['id']; ?>"> <button class="btn_comprar">Agregar al carrito</button></a>
                    </div>


                </div>


                <div class="cajasinopsis">
                    <p><?php echo $registro1['sinopsis'] ?></p>
                </div>


                <!-- caja género -->
                <?php

                        $varGenero = $registro1['idgenero']; //este debe ser un int para que la comparacion funcione

                        $sql2 = "SELECT * FROM mangacatalogo WHERE idgenero = " . $varGenero . " order by RAND() LIMIT 4";

                        $consulta2 = mysqli_query($conexion, $sql2);

                        ?>


                <div class="productos">

                    <?php

                            if (mysqli_num_rows($consulta2) > 0) {


                                while ($registro2 = mysqli_fetch_assoc($consulta2)) {

                                    ?>

                            <article><a href="producto.php?producto=<?php echo $registro2['id']; ?>"><img src="images/<?php echo $registro2['portada'] ?>" /></a>
                                <h2><?php echo $registro2['nombre'] ?></h2>
                                <h4><?php echo $registro2['autor']; ?></h4>
                                <h3>$<?php echo $registro2['precio']; ?></h3>
                            </article>



                        <?php
                                    }
                                    ?>
                </div>

            <?php

                    }
                    ?>
    </div>
<?php
    }
    ?>
</div>
<?php
} else {
    echo "No existe este producto";
}
?>





<?php include("includes/footer.html"); ?>

<script type="text/javascript" src="js/footer.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
</body>

</html>