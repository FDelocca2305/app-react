<!DOCTYPE html>
<html>

<head>
	<title>Inicio | Portal Comics</title>
	<link rel="stylesheet" type="text/css" href="css/slider.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/header.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link rel="stylesheet" type="text/css" href="css/whatsapp.css">
	<link rel="icon" type="image/png" href="/images/icons/LogoPestaña.png" />
	<link rel="stylesheet" type="text/css" href="css/carrito.css">
	<link rel="stylesheet" type="text/css" href="css/productosReco.css">
	<link rel="stylesheet" type="text/css" href="css/indexProductos.css">
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/png" href="images/icons/LogoPestaña.png"/>
	<link rel="stylesheet" type="text/css" href="fonts/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>

<body>

	<header>
		<?php
		session_start();
		require("includes/header.php") ?>
		<?php if (isset($_GET['buscar'])) {
			$buscar = $_GET['buscar'];
			header('Location: catalogo.php?');
		}
		?>
	</header>

	<?php require("includes/menu.php") ?>
	<div class="empty">
		<div class="swal-icon swal-icon--success">
			<span class="swal-icon--success__line swal-icon--success__line--long"></span>
			<span class="swal-icon--success__line swal-icon--success__line--tip"></span>

			<div class="swal-icon--success__ring"></div>
			<div class="swal-icon--success__hide-corners"></div>
			<?php


			include('includes/carrito/libreria.php');
			comprar();
			?>
		</div>
		<div class="swal-title" style="">Se ha realizado la compra</div>
		<div>
			<a href="catalogo.php"><i class="fas fa-book-open fa-2x"></i><span> Volver al catalogo</span></a>
		</div>
	</div>
	<div>
	</div>

	<?php require("includes/whatsapp.php") ?>
	<?php require("includes/footer.html") ?>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="js/footer.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script>
		/* $(document).ready(function() {
            swal("Se ha realizado la compra", "Será devuelto al catalogo", "success");
            sleep(1);
            window.location.href = "catalogo.php";
        }); */
	</script>
</body>

</html>